#
# Phase plot of system y_2 = f(y_1)
# Argument is the <result>.dat file
#
filename=$1
bname=$(basename $filename .dat)
cat >${bname}_phase.gnuplot <<EOF
set grid
set xlabel "y_1"
set ylabel "y_2"
set title "Phase plot of $bname"
unset key
plot "$filename" using 2:3 with lines lc 6
EOF
gnuplot ${bname}_phase.gnuplot -
