(** ODE integration by analytic continuation
    using Taylor series expansion *)
open Sexplib

(** Exception to be raised when an ODE definition is not valid
    in an S-expression file *)
exception Invalid_ode_format of string

(** Type to define an expression *)
type t =   Const of float  (** constant *)
         | Var of int*int  (** (i,d): variable number i, d-th derivate *)
         | Neg of t        (** opposite *)
         | Add of t*t      (** add *)
         | Sub of t*t      (** substract *)
         | Mul of t*t      (** product *)
         | Div of t*t      (** divide *)
         | Pow of t*t      (** raise to the power *)
         | Sqr of t        (** square *)
         | Sqrt of t       (** square root *)
         | Exp of t        (** exponential *)
         | Ln of t         (** Neper logarithm *)
         | Log10 of t      (** decimal logarithm *)
         | Sin of t        (** sine *)
         | Cos of t        (** cosine *)
         | Tan of t        (** tangent *)
         | Sinh of t       (** hyperbolic sine *)
         | Cosh of t       (** hyperbolic cosine *)
         | Tanh of t       (** hyperbolic tangent *)

(** Evaluate an expresson in the context of variable
    @param xpr a {!t} expression
    @param vars a float list list. A list of variables, each variable
    is a list of the value of the 0th, 1st, 2nd, derivatives
    @return a float
  *)
let rec eval xpr vars =
   match xpr with
   | Const v    -> v
   | Var (i, d) -> List.nth (List.nth vars i) d
   | Neg x -> ~-. (eval x vars)
   | Add (a, b) -> (eval a vars) +. (eval b vars)
   | Sub (a, b) -> (eval a vars) -. (eval b vars)
   | Mul (a, b) -> (eval a vars) *. (eval b vars)
   | Div (a, b) -> (eval a vars) /. (eval b vars)
   | Pow (a, b) -> (eval a vars) ** (eval b vars)
   | Sqr x -> let y = eval x vars in y *. y
   | Sqrt x -> sqrt (eval x vars)
   | Exp x -> exp (eval x vars)
   | Ln x -> log (eval x vars)
   | Log10 x -> log10 (eval x vars)
   | Sin x -> sin (eval x vars)
   | Cos x -> cos (eval x vars)
   | Tan x -> tan (eval x vars)
   | Sinh x -> sinh (eval x vars)
   | Cosh x -> cosh (eval x vars)
   | Tanh x -> tanh (eval x vars)

(** Try to evaluate a constant expression, i.e. that does not depend
    on variables
    @param expr a {!t} expression
    @param fconst a float -> {!t} function that is called with the
    evaluated constant value and returns a new {!t} expression
    @param default a {!t} expression to be returned if {e expr} is
    not a constant
    @return a new {!t} expression, either computed by {e fconst} if
    constant evaluation is successful, or the {e default} expression
  *)
let if_eval_const expr fconst default =
   try
      let v = eval expr [] in fconst v
   with Invalid_argument _ -> default

(** Apply a Neg operator before a negative constant
    @param v a float value
    @return a {!t} expression
  *)
let apply_neg v =
   if v < 0.
   then Neg (Const (abs_float v))
   else Const v

(** Differentiate an expression
    @param xpr a {!t} expression
    @return a new {!t} expression differentiated
  *)
let rec deriv xpr =
   match xpr with
   | Const v -> Const 0.0
   | Var (i, d) -> Var (i, d+1)
   | Neg u -> Neg (deriv u)
   | Add (u, v) -> Add (deriv u, deriv v)
   | Sub (u, v) -> Sub (deriv u, deriv v)
   | Mul (u, v) -> Add (Mul (deriv u, v), Mul (u, deriv v))
   | Div (u, v) -> Div (Sub (Mul (deriv u, v), Mul (u, deriv v)), Sqr v)
   | Pow (u, v) -> (* optimization: distinguish constant/non-constant power *)
         if_eval_const v
            (function c -> Mul (deriv u,
                                Mul (Const c, Pow (u, apply_neg (c -. 1.)))))
            (Mul (Pow (u, v), Add (Mul (deriv v, Ln u),
                                   Mul (v, deriv (Ln u)))))
   | Sqr u -> Mul (Const 2.0, Mul (deriv u, u))
   | Sqrt u -> Div (deriv u, (Mul (Const 2.0, Sqrt u)))
   | Exp u -> Mul (deriv u, Exp u)
   | Ln u -> Div (deriv u, u)
   | Log10 u -> let m10 = 1.0 /. (log 10.0) in
                Mul (Const m10, Div (deriv u, u))
   | Sin u -> Mul (deriv u, Cos u)
   | Cos u -> Neg (Mul (deriv u, Sin u))
   | Tan u -> Mul (deriv u, Add (Const 1.0, Sqr (Tan u)))
   | Sinh u -> Mul (deriv u, Cosh u)
   | Cosh u -> Mul (deriv u, Sinh u)
   | Tanh u -> Mul (deriv u, Sub (Const 1.0, Sqr (Tanh u)))

(** Repeat the application of a function
    @param f a 'a -> 'a function
    @param n an int, the number of applications
    @param dat a 'a initial data
    @return a 'a value, the result of the function f being
    applied n times
  *)
let rec repeat f n dat =
   if n = 1
   then f dat
   else f (repeat f (n-1) dat)

(** Trivial simplification of an expression
    @param xprs a {!t} expression
    @return a simplified {!t} expression
  *)
let trivial xprs =
   let rec aux xpr =
      match xpr with
      | Neg (Neg x) -> x
      | Neg (Const x) when x <= 0.0 -> Const (abs_float x)
      | Add (Const 0.0, x)
      | Add (x, Const 0.0)  -> aux x
      | Sub (Const 0.0, x)  -> Neg (aux x)
      | Sub (x, Const 0.0)  -> aux x
      | Mul (Const 0.0, x)
      | Mul (x, Const 0.0)  -> Const 0.0
      | Mul (Const 1.0, x)
      | Mul (x, Const 1.0)  -> aux x
      | Div (Const 0.0, x) -> Const 0.0
      | Pow (Const 0.0, Const 0.0) -> Const 1.0
      | Pow (x, Const 0.0) -> Const 1.0
      | Pow (x, Const 1.0) -> aux x
      | Sqr (Const 0.0) -> Const 0.0
      | Sqr (Const 1.0) -> Const 1.0
      | Sqrt (Const 0.0) -> Const 0.0
      | Sqrt (Const 1.0) -> Const 1.0
      | Exp (Const 0.0) -> Const 1.0
      (* followed by the other non-simplified cases *)
      | Const x -> Const x
      | Var (i,d) -> Var (i,d)
      | Neg x -> Neg (aux x)
      | Add (a, b) -> Add (aux a, aux b)
      | Sub (a, b) -> Sub (aux a, aux b)
      | Mul (a, b) -> Mul (aux a, aux b)
      | Div (a, b) -> Div (aux a, aux b)
      | Pow (a, b) -> Pow (aux a, aux b)
      | Sqr x -> Sqr (aux x)
      | Sqrt x -> Sqrt (aux x)
      | Exp x -> Exp (aux x)
      | Ln x -> Ln (aux x)
      | Log10 x -> Log10 (aux x)
      | Sin x -> Sin (aux x)
      | Cos x -> Cos (aux x)
      | Tan x -> Tan (aux x)
      | Sinh x -> Sinh (aux x)
      | Cosh x -> Cosh (aux x)
      | Tanh x -> Tanh (aux x)
   in
   (* Apply six times! Dirty and ugly! *)
   repeat aux 6 xprs

(** Substitute the derivatives of the variables with the constant 1,
    in order to prevent further differentation with respect with
    the variables!
    @param xprs a {!t} expression
    @return a new {!t} expression
  *)
let rec noderiv xpr =
   match xpr with
   | Const x -> Const x
   | Var (i,d) -> if d > 0 then Const 1.0 else Var (i,d)
   | Neg x -> Neg (noderiv x)
   | Add (a, b) -> Add (noderiv a, noderiv b)
   | Sub (a, b) -> Sub (noderiv a, noderiv b)
   | Mul (a, b) -> Mul (noderiv a, noderiv b)
   | Div (a, b) -> Div (noderiv a, noderiv b)
   | Pow (a, b) -> Pow (noderiv a, noderiv b)
   | Sqr x -> Sqr (noderiv x)
   | Sqrt x -> Sqrt (noderiv x)
   | Exp x -> Exp (noderiv x)
   | Ln x -> Ln (noderiv x)
   | Log10 x -> Log10 (noderiv x)
   | Sin x -> Sin (noderiv x)
   | Cos x -> Cos (noderiv x)
   | Tan x -> Tan (noderiv x)
   | Sinh x -> Sinh (noderiv x)
   | Cosh x -> Cosh (noderiv x)
   | Tanh x -> Tanh (noderiv x)

(** Split a string in blocks of at most n characters
    Blocks are terminated by '&' FORTRAN line-continuation symbol,
    followed by end-of-line.
    @param n an int value, the maximum length of a block
    @param s a string
    @return a new string s, splitted in blocks
  *)
let rec string_blocks n s =
   let slen = String.length s
   and is_alphanum c = (Char.compare c 'a') >= 0 && (Char.compare c 'z') <= 0
                       || (Char.compare c '0') >= 0 && (Char.compare c '9') <= 0
                       || (Char.compare c '.') == 0
                       || (Char.compare c '*') == 0
   in
   if slen > n
   then begin
           (* do not split identifiers! *)
           let char_before = is_alphanum (String.get s (n-1))
           and char_after = is_alphanum (String.get s n) in
           let k =
              if char_before && char_after
              then String.rindex (String.sub s 0 n) ' '
              else n 
           in
           (* get one block of at most k characters *)
           (String.sub s 0 k) ^ " &\n"
           ^ (string_blocks n (String.sub s k (slen-k)))
        end
   else s (* short string, do not split *)

(** Write the expression into a string with infix notation,
    and FORTRAN-like syntax.
    Manage the following operation priority levels:
    -  Const, Var 6
    -  functions  5
    -  Pow, Sqr   4
    -  Mul, Div   3
    -  Neg        2
    -  Add, Sub   1
    @param xprs a {!t} expression
    @return a string
  *)
let to_string xprs =
   let rec aux xpr =
      let par curlvl (sublvl, s) =
         (* enclose with parenthesis according to priority levels *)
         if sublvl > curlvl then s else "(" ^ s ^ ")"
      and epar (_,s) = "(" ^ s ^ ")"  (* enforce parenthesis *)
      in
      match xpr with
      | Const v    -> 6, if v < 0.
                         then "(" ^ string_of_float v ^ ")"
                         else string_of_float v
      | Var (i, d) -> 6, "y(" ^ (string_of_int (i+1)) ^ "," ^ (string_of_int d) ^ ")"
      | Neg x -> 2, "-" ^ (par 2 (aux x))
      | Add (u, v) -> 1, (par 1 (aux u)) ^ " + " ^ (par 1 (aux v))
      | Sub (u, v) -> 1, (par 1 (aux u)) ^ " - " ^ (par 1 (aux v))
      | Mul (u, v) -> 3, (par 3 (aux u)) ^ " * " ^ (par 3 (aux v))
      | Div (u, v) -> 3, (par 3 (aux u)) ^ " / " ^ (par 3 (aux v))
      | Pow (u, v) -> 4, (par 4 (aux u)) ^ "**" ^ (par 4 (aux v))
      | Sqr u -> 4, (par 4 (aux u)) ^ "**2"
      | Sqrt u -> 5, "sqrt" ^ (epar (aux u))
      | Exp u ->  5, "exp" ^ (epar (aux u))
      | Ln u -> 5, "log" ^ (epar (aux u))
      | Log10 u -> 5, "log10" ^ (epar (aux u))
      | Sin u -> 5, "sin" ^ (epar (aux u))
      | Cos u -> 5, "cos" ^ (epar (aux u))
      | Tan u -> 5, "tan" ^ (epar (aux u))
      | Sinh u -> 5, "sinh" ^ (epar (aux u))
      | Cosh u -> 5, "cosh" ^ (epar (aux u))
      | Tanh u -> 5, "tanh" ^ (epar (aux u))
   in
   let _, s = aux (trivial xprs) in string_blocks 72 s

(** Read an S-expression file that defines an ODE system
    @param a {!Sexp.t} list of s-expressions read from a file
    @param params is a (string, float) Hashtbl.t of parameters
    @return a new {!t} list, a list of expressions that define
    the right-hand side of an ODE system. The number of elements
    of the list is the dimension of the system
  *)
let parse_ode_sexpr ode_sexpr params =
   let parse_ode i sexpr =
      let rerror s =
         raise (Invalid_ode_format (s ^ " in equation no. "
                                    ^ (string_of_int (i+1))))
      and var_match = Str.regexp "y\\([1-9]\\)"
      in
      let rec parse_fcn expr =
         match expr with
         | Sexp.List [Sexp.Atom "-"; op] -> Neg (parse_fcn op)
         | Sexp.List [Sexp.Atom "+"; op1; op2]
               -> Add (parse_fcn op1, parse_fcn op2)
         | Sexp.List [Sexp.Atom "-"; op1; op2]
               -> Sub (parse_fcn op1, parse_fcn op2)
         | Sexp.List [Sexp.Atom "*"; op1; op2]
               -> Mul (parse_fcn op1, parse_fcn op2)
         | Sexp.List [Sexp.Atom "/"; op1; op2]
               -> Div (parse_fcn op1, parse_fcn op2)
         | Sexp.List [Sexp.Atom "**"; op1; op2]
               -> Pow (parse_fcn op1, parse_fcn op2)
         | Sexp.List [Sexp.Atom "sqr"; op] -> Sqr (parse_fcn op)
         | Sexp.List [Sexp.Atom "sqrt"; op] -> Sqrt (parse_fcn op)
         | Sexp.List [Sexp.Atom "exp"; op] -> Exp (parse_fcn op)
         | Sexp.List [Sexp.Atom "ln"; op] -> Ln (parse_fcn op)
         | Sexp.List [Sexp.Atom "log"; op] -> Ln (parse_fcn op)
         | Sexp.List [Sexp.Atom "log10"; op] -> Log10 (parse_fcn op)
         | Sexp.List [Sexp.Atom "sin"; op] -> Sin (parse_fcn op)
         | Sexp.List [Sexp.Atom "cos"; op] -> Cos (parse_fcn op)
         | Sexp.List [Sexp.Atom "tan"; op] -> Tan (parse_fcn op)
         | Sexp.List [Sexp.Atom "sinh"; op] -> Sinh (parse_fcn op)
         | Sexp.List [Sexp.Atom "cosh"; op] -> Cosh (parse_fcn op)
         | Sexp.List [Sexp.Atom "tanh"; op] -> Tanh (parse_fcn op)
         | Sexp.Atom s ->
            if Str.string_match var_match s 0
            then Var ((int_of_string (Str.matched_group 1 s))-1, 0)
            else if Hashtbl.mem params s
            then Const (Hashtbl.find params s)
            else (try Const (float_of_string s)
                  with Failure _ -> rerror "invalid float")
         | _ -> rerror "invalid expression"
      in
      parse_fcn sexpr
   in
   (* Main expression of the parse_ode_sexpr function *)
   List.mapi parse_ode ode_sexpr

(** Generate the FORTRAN code for computing a Taylor serie step
    @param modulename a string, the name of the FORTRAN module to create
    @param fcn a {!t} list, the expressions that define the ODE system
    @param p an int value, the maximum derivation order of the Taylor serie
    @return ()
  *)
let generate_Taylor modulename fcn p =
   let fh = open_out (modulename ^ ".f95") in
   let printy d i y = (* print d-th derivative of component i of y *)
      let s = to_string y in
      if (String.length s) > 80
      then Printf.fprintf fh "y(%d,%d) = &\n%s\n" (i+1) d (to_string y)
      else Printf.fprintf fh "y(%d,%d) = %s\n" (i+1) d (to_string y)
   in
   let rec aux y d =
      if d = 1
      then begin (* y' = f(y) *)
              List.iteri (printy d) y;
              aux y (d+1); 
           end
      else begin (* higher-order derivatives *)
              if d <= p
              then begin
                      (* differentiate all components of y *)
                      let yd = List.map deriv y in
                      List.iteri (printy d) yd;
                      aux yd (d+1); 
                   end
              else ()
           end
   in
   (* subroutine declaration *)
   Printf.fprintf fh
     "module %s\ncontains\n\
      subroutine taylor_step(n,y0,h,yh)\n\
      implicit none\n\
      integer, intent(in) :: n\n\
      integer, parameter :: p = %d\n\
      double precision, dimension(n), intent(in) :: y0\n\
      double precision, intent(in) :: h\n\
      double precision, dimension(n), intent(out) :: yh\n\
      integer :: i\n\
      integer, dimension(0:p) :: fact\n\
      double precision, dimension(n,0:p) :: y\ny(:,0) = y0\n"
      modulename p;
   (* generate code *)
   aux fcn 1;
   (* compute factorial and Horner algorithm *)
   Printf.fprintf fh
     "fact(0) = 1D0\n\
      do i = 1, p\n   \
         fact(i) = fact(i-1)*i\n\
      enddo\n\
      yh = 0D0\n\
      do i = p, 0, -1\n   \
         yh = h*yh + y(:,i)/fact(i)\n\
      enddo\n\
      end subroutine\n\
      end module %s\n" modulename;
   close_out fh
