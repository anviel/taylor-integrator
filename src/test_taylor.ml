(** Test the Taylor module *)
open Taylor

let test1 _ =
   let xpr0 = Add (Div (Const 5.0, Sqrt (Var (0,0))), Sqr (Sin (Var (0,0))))
   and x = 1.34 in
   let xpr1 = noderiv (deriv xpr0) in
   let xpr2 = noderiv (deriv xpr1) in
   print_endline ("f = " ^ (to_string xpr0));
   print_float (eval xpr0 [ [ x ] ]);
   print_endline ("\nf' = " ^ (to_string xpr1));
   print_float (eval xpr1 [ [ x ] ]);
   print_endline ("\nf'' = " ^ (to_string xpr2));
   print_float (eval xpr2 [ [ x ] ]);
   print_endline "";

and test2 _ =
   (* Non-linear simple pendulum ODE *)
   let f = [ 
             Var (1,0);
             Neg (Sin (Var (0,0)))
           ]
   in
   generate_Taylor "pendulum" f 5

;;

test2 ()
