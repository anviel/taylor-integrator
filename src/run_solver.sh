#
# Run the Taylor series-based ODE solver
#
filename=$1
sysname=$(basename $1 .sex)
# generate code for Taylor serie expansion
if ./taylor $filename;
then
# compile Taylor computation module and driver
gfortran -c ${sysname}.f95
gfortran -c ${sysname}_integ.f95
gfortran -o ${sysname} ${sysname}.f95 ${sysname}_integ.f95
# run simulation
./${sysname} >${sysname}.dat
# post-process results
gnuplot ${sysname}.gnuplot -
fi
