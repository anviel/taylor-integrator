(** Main program of the Taylor-series based ODE integrator *)
open Sexplib

(** A type to hold the simulation run parameters *)
type run_parameters = {
   nbstep  : int; (** number of fixed steps *)
   stepsize: float; (** fixed step size [s] *)
   maxorder: int  (** maximum order of Taylor serie *)
}

(** Default values for the run parameters *)
let default_run_parameters =
   {
      nbstep = 100;
      stepsize = 0.1;
      maxorder = 4
   }

(** Exception to be raised if an S-expression is invalid *)
exception Invalid_file_format of string

let rec join sep ls =
   match ls with
   | []  -> ""
   | [a] -> a
   | a :: tl -> a ^ sep ^ (join sep tl)

(** Read an S-expression file that defines an ODE system
    @param filename a string, the file to be read and parsed
    @return a tuple {e (ode_sys, init, params, run_params)} with
    {e ode_sys} a {!Sexp.t} list of s-expressions that define
    the right-hand side of an ODE system;
    {e init} is a float list of the ODE initial conditions;
    {e params} is a (string, float) Hashtbl.t of parameters;
    {e run_params} is a {!run_parameters} record.
  *)
let read_sexpr_file filename =
   let parse_sexpr_file (i, ode_sys, init, params, run_params) sexpr =
      let rerror s =
         raise (Invalid_file_format (s ^ " at line " ^ (string_of_int i)
                                     ^ " of " ^ filename))
      in
      match sexpr with
      | Sexp.List [ Sexp.Atom "ode"; xprl ] ->
            i+1, xprl :: ode_sys, init, params, run_params
      | Sexp.List [ Sexp.Atom "init"; Sexp.Atom initv ] ->
            i+1, ode_sys,
            (try float_of_string initv
             with Failure _ -> rerror "invalid initial value") :: init,
            params, run_params
      | Sexp.List [ Sexp.Atom "nbstep"; Sexp.Atom nb ] ->
            i+1, ode_sys, init, params,
            { run_params with nbstep =
                  try int_of_string nb
                  with Failure _ -> rerror "invalid nbstep value"
            }
      | Sexp.List [ Sexp.Atom "stepsize"; Sexp.Atom sts ] ->
            i+1, ode_sys, init, params,
            { run_params with stepsize =
                  try float_of_string sts
                  with Failure _ -> rerror "invalid stepsize value"
            }
      | Sexp.List [ Sexp.Atom "maxorder"; Sexp.Atom smxo ] ->
            i+1, ode_sys, init, params,
            { run_params with maxorder =
                  let mxo =
                     try int_of_string smxo
                     with Failure _ -> rerror "invalid maxorder value"
                  in
                  if mxo >= 1 || mxo <= 8
                  then mxo
                  else rerror "maxorder must be between 1 and 8"
            }
      | Sexp.List [ Sexp.Atom "param"; Sexp.Atom pname; Sexp.Atom pvalue ] ->
           i+1, ode_sys, init,
           (try Hashtbl.add params pname (float_of_string pvalue); params
            with Failure _ -> rerror ("invalid value for parameter " ^ pname)),
           run_params 
      | _ -> rerror "invalid declaration"
   in
   (* Main expression of the read_sexpr_file function *)
   let f = open_in filename in
   let exprl = Sexp.input_sexps f in
   close_in f;
   let _, ode_sys, init, params, run_params =
      List.fold_left 
         parse_sexpr_file
         (1, [], [], (Hashtbl.create 10), default_run_parameters)
         exprl
   in
   if (List.length init) <> (List.length ode_sys)
   then raise (Invalid_file_format ("Number of initial condtions \
               does not match the number of ODE"))
   else List.rev ode_sys, List.rev init, params, run_params

(** Generate the code for the driver solver
    @param sys_name a string, the system name
    @param ndim an int value, the system dimension (= number of
    state variables and differential equations)
    @param init a float list, the initial conditions
    @param run_params a {!run_parameters} record
    @return ()
  *)
and generate_solver_driver sys_name ndim init run_params =
   let program_name = sys_name ^ "_integ" in
   let f = open_out (program_name ^ ".f95") in
   Printf.fprintf f "program %s\n\
                     use %s\n\
                     double precision, dimension(%d) :: y0 = "
                     program_name sys_name ndim;
   Printf.fprintf f "(/ %s /)\n" (join ", " (List.map string_of_float init));
   Printf.fprintf f "double precision, dimension(%d) :: yh\n\
                     double precision :: h = %f\n\
                     integer :: i\n\
                     print 4700, 0D0, y0\n\
                     do i = 1, %d\n   \
                        call taylor_step(%d, y0, h, yh)\n   \
                        print 4700, i*h, yh\n   \
                        y0 = yh\n\
                     enddo\n\
                     4700 format(f7.3, %d(1x, e25.16))\n\
                     end program %s\n"
                     ndim run_params.stepsize run_params.nbstep
                     ndim ndim program_name;
   close_out f

(** Generate the code for post-processing the results in Gnuplot
    @param sys_name a string, the system name
    @param ndim an int value, the system dimension (= number of
    state variables and differential equations)
    @return ()
  *)
and generate_post_process sys_name ndim =
   let f = open_out (sys_name ^ ".gnuplot") in
   for i = 1 to ndim do
      Printf.fprintf
         f
         "set grid\n\
          set xlabel \"t [s]\"\n\
          set ylabel \"y%d\"\n\
          plot \"%s\" using 1:%d title \"y%d\" with lines lc 6\n\
          pause -1\n"
         i (sys_name ^ ".dat") (i+1) i;
   done;
   close_out f

(** Main program *)
let main _ =
   if Array.length Sys.argv <> 2
   then
   begin
      print_endline "usage: run_solver <ode_file.sex>";
      exit 1
   end
   else
   begin
      let filename = Sys.argv.(1) in
      let sys_name = String.sub filename 0 ((String.length filename)-4) in
      let ode_sexpr, init, params, run_params = read_sexpr_file filename in
      let ode_sys = Taylor.parse_ode_sexpr ode_sexpr params in
      Taylor.generate_Taylor sys_name ode_sys run_params.maxorder;
      generate_solver_driver
         sys_name
         (List.length ode_sys)
         init
         run_params;
      generate_post_process sys_name (List.length ode_sys)
   end

;;
main ()
