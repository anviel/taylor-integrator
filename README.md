# Taylor integrator

A Taylor series based numerical integration tool of system of ordinary differential equations (ODE), in explicit first order form.


## Dependencies

- [Ocaml](https://www.ocaml.org) >= 4.02
- [Sexplib](https://opam.ocaml.org/packages/sexplib)
- [GNU Fortran](http://gcc.gnu.org/fortran/)
- [gnuplot](http://www.gnuplot.info)

## Build
In the `src` directory, simply launch `make`, this should produce a `taylor` binary.

## Getting started
ODE systems are described in [S-expression](https://en.wikipedia.org/wiki/S-expression) formatted files.
The tool is a command-line tool only.
Take the `examples/vanderpol.sex` file:
```
(stepsize 0.1)
(nbstep 200)
(init 0.1)
(init 0.1)
(param mu 0.2)
(ode y2)
(ode (- (* mu (- 1.0 (sqr y1))) y1))
```

Call the script to analyze it, generate code, run simulation and post-process results:

```
./run_solver.sh vanderpol.sex
```

This starts the *taylor* analyzer which generates code in two Fortran 95 files:

```
vanderpol.f95
vanderpol_integ.f95
```

These files are compiled into the executable binary `vanderpol` which executes the simulation, and starts gnuplot to plot the two state variables *y1* and *y2*.
Results are stored in a file `vanderpol.dat` which can be later used to plot the phase portrait of the system:

```
./phase_plot.sh vanderpol.dat
```
![Van Der Pol oscillator](images/vanderpol.png)

## Principle of the method
A system of first order explicit ordinary differential equations can be written as:
![ode](images/first_order_explicit_ode.png)

With the initial condition
![initial condition](images/initial_condition.png)
this defines an *initial value problem*, which solution can be expressed as a Taylor serie at *t0*:
![Taylor serie](images/taylor_serie.png)

The first term is defined by the initial condition, and the second term by the differential equation. The third term, of order 2 in *h*, is obtained by differentiating the differential equation:
![second derivative](images/second_derivative.png)
By differentiating this term again, and again, the other terms of the serie can be computed.

When enough terms are available, the partial serie is an approximation of the solution at *t0+h*. The process can then be repeated, by computing a new Taylor serie at this point.

## System description format
ODE systems are described in [S-expression](https://en.wikipedia.org/wiki/S-expression) formatted files.
Each declaration is an enclosed expression starting with a keyword:

- `nbstep`  followed by an integer value, defines the number of steps of integration. Default value is 100
- `stepsize` followed by a float value, defines the step size in arbitrary unit. Default value is 0.1
- `maxorder` followed by an integer value, defines the maximum order of the Taylor expansion, minimum value is 1 and maximum is set to 8. Default value is 4
- `param` followed by an identifier (that cannot starts by *y*) and a float value, defines a parameter to be substituted into the differential equations
- `init` followed by a float value, defines the initial condition of a state variable. First `init` declaration defines the initial condition for the first state variable, second `init` defines the initial condition for the second state variable, etc.
- `ode` followed by an expression, defines the right-hand side of an explicit first-order ordinary differential equation. A maximum of 9 differential equations is allowed.

Order of declarations do not matter except for the `init` statements that relate to the numbering of the state variables (`y1`, `y2`, ...) which appear on the `ode` declarations.

An ODE is an expression, built using the following primitives:
- float numbers, possibly using scientific notation
- state variables: `y1`, `y2`, ... up to `y9`. Obviously there must be no more state variables than differential equations
- unary operator: -
- binary operators: + - \* / and \*\* for exponentiation
- functions: `sqr` (square), `sqrt` (square root), `exp` (exponential), `ln` or `log` (Neper logarithm), `log10` (decimal logarithm), `sin` (sine), `cos` (cosine), `tan` (tangent), `sinh` (hyperbolic sine), `cosh` (hyperbolic cosine), `tanh` (hyperbolic tangent)
- any parameter name defined by a `param` statement


## Example
The above Van Der Pol oscillator from the `examples` directory:
```
(stepsize 0.1)
(nbstep 200)
(init 0.1)
(init 0.1)
(param mu 0.2)
(ode y2)
(ode (- (* mu (- 1.0 (sqr y1))) y1))
```
The integration is performed using 200 steps of 0.1 second. Starting implicitly from 0 second as initial time, simulation horizon is thus 20 seconds.
The system has two state variables `y1` and `y2`, and the initial conditions are *y1 = 0.1* and *y2 = 0.1*.
There are consequently two ordinary differential equations:
- the first ODE reads as _y1' = y2_
- the second ODE reads as _y2' = mu (1 - y1\*y1) - y1_
where `mu` is a parameter, which value 0.2, is substituted into the equation.
